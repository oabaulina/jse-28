package ru.baulina.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestName;
import ru.baulina.tm.IntegrationTest;

import java.util.ArrayList;
import java.util.List;

@Category(IntegrationTest.class)
public class TaskEndpointIT {

//    @NotNull static private SessionDTO sessionTest;
//
//    @Rule
//    public final TestName testName = new TestName();
//
//    @NotNull
//    private static final SessionEndpointService sessionEndpointService = new SessionEndpointService();
//    @NotNull
//    private static final SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();
//    @NotNull
//    private static final TaskEndpointService taskEndpointService = new TaskEndpointService();
//    @NotNull
//    private static final TaskEndpoint taskEndpoint = taskEndpointService.getTaskEndpointPort();
//
//    @Before
//    public void setTestName() throws Exception {
//        System.out.println(testName.getMethodName());
//    }
//
//    @After
//    public void clearTask() throws Exception {
//        taskEndpoint.clearTasks(sessionTest);
//    }
//
//    @BeforeClass
//    public static void setSessions() throws Exception {
//        sessionTest = sessionEndpoint.openSession("test", "test");
//    }
//
//    @AfterClass
//    public static void closeSessions() throws Exception {
//        sessionEndpoint.closeSession(sessionTest);
//    }
//
//    @Test
//    public void testCreateTaskWithDescription() {
//        @Nullable TaskDTO task = taskEndpoint.createTaskWithDescription(
//                sessionTest, 123L,"task", "description"
//        );
//        Assert.assertNotNull(task);
//        Assert.assertEquals("task", task.getName());
//        Assert.assertEquals("description", task.getDescription());
//   }
//
//    @Test
//    public void testFindAllTasks() {
//        @NotNull TaskDTO task1 = taskEndpoint.createTask(sessionTest, 123L,"task1");
//        @NotNull TaskDTO task2 = taskEndpoint.createTask(sessionTest, 123L,"task2");
//        @NotNull final List<TaskDTO> listTaskExpected = taskEndpoint.findAllTasks(sessionTest);
//        Assert.assertEquals(2, listTaskExpected.size());
//        Assert.assertEquals(task1.getId(), listTaskExpected.get(0).getId());
//        Assert.assertEquals(task2.getId(), listTaskExpected.get(1).getId());
//     }
//
//    @Test
//    public void testFindOneTaskByIndex() {
//        @NotNull TaskDTO task1 = taskEndpoint.createTask(sessionTest, 123L,"task1");
//        @NotNull TaskDTO task2 = taskEndpoint.createTask(sessionTest, 123L,"task2");
//        Assert.assertEquals(task1.getId(), taskEndpoint.findOneTaskByIndex(sessionTest, 123L,0).getId());
//        Assert.assertEquals(task2.getId(), taskEndpoint.findOneTaskByIndex(sessionTest, 123L,1).getId());
//    }
//
//    @Test
//    public void testLoadTasks() {
//    }
//
//    @Test
//    public void testClearTasks() {
//        taskEndpoint.createTask(sessionTest, 123L,"task1");
//        taskEndpoint.createTask(sessionTest, 123L,"task2");
//        Assert.assertEquals(2, taskEndpoint.findAllTasks(sessionTest).size());
//        taskEndpoint.clearTasks(sessionTest);
//        Assert.assertEquals(0, taskEndpoint.findAllTasks(sessionTest).size());
//    }
//
//    @Test
//    public void testFindOneTaskById() {
//        @NotNull TaskDTO task1 = taskEndpoint.createTask(sessionTest, 123L,"task1");
//        @NotNull final Long id1 = task1.getId();
//        @NotNull TaskDTO task3 = taskEndpoint.findOneTaskById(sessionTest, 123L, id1);
//        @NotNull final Long id3 = task3.getId();
//        Assert.assertEquals(id1, id3);
//   }
//
//    @Test
//    public void testRemoveOneTaskById() {
//        taskEndpoint.createTask(sessionTest, 123L,"task1");
//        taskEndpoint.createTask(sessionTest, 123L,"task2");
//        @Nullable TaskDTO task1 = taskEndpoint.findOneTaskByName(sessionTest, 123L,"task1");
//        @Nullable TaskDTO task2 = taskEndpoint.findOneTaskByName(sessionTest, 123L,"task2");
//        Assert.assertNotNull(task1);
//        Assert.assertNotNull(task2);
//        @NotNull final Long id1 = task1.getId();
//        Assert.assertNotNull(taskEndpoint.findOneTaskById(sessionTest, 123L,id1));
//        taskEndpoint.removeOneTaskById(sessionTest, 123L,task1.getId());
//        Assert.assertNull(taskEndpoint.findOneTaskById(sessionTest, 123L,id1));
//        @NotNull final Long id2 = task2.getId();
//        Assert.assertNotNull(taskEndpoint.findOneTaskById(sessionTest, 123L,id2));
//        taskEndpoint.removeOneTaskById(sessionTest, 123L,task2.getId());
//        Assert.assertNull(taskEndpoint.findOneTaskById(sessionTest, 123L,id2));
//    }
//
//    @Test
//    public void testUpdateTaskById() {
//        taskEndpoint.createTask(sessionTest, 123L,"task");
//        @NotNull TaskDTO task = taskEndpoint.findOneTaskByName(sessionTest, 123L,"task");
//        @NotNull final Long id = task.getId();
//        taskEndpoint.updateTaskById(
//                sessionTest, 123L, id, "taskNew", "descriptionNew"
//        );
//        Assert.assertEquals("taskNew", taskUpdate.getName());
//        Assert.assertEquals("descriptionNew", taskUpdate.getDescription());
//    }
//
//    @Test
//    public void testUpdateTaskByIndex() {
//        taskEndpoint.createTask(sessionTest, 123L,"task");
//        taskEndpoint.updateTaskByIndex(
//                sessionTest, 123L,0, "taskNew", "descriptionNew"
//        );
//        Assert.assertEquals("taskNew", task.getName());
//        Assert.assertEquals("descriptionNew", task.getDescription());
//    }
//
//    @Test
//    public void testRemoveTask() {
//        @Nullable TaskDTO task1 = taskEndpoint.createTask(sessionTest, 123L,"task1");
//        @Nullable TaskDTO task2 = taskEndpoint.createTask(sessionTest, 123L,"task2");
//        Assert.assertEquals(2, taskEndpoint.getTaskList().size());
//        taskEndpoint.removeTask(sessionTest, task1);
//        Assert.assertEquals(1, taskEndpoint.getTaskList().size());
//        taskEndpoint.removeTask(sessionTest, task2);
//        Assert.assertEquals(0, taskEndpoint.getTaskList().size());
//    }
//
//    @Test
//    public void testRemoveOneTaskByIndex() {
//        taskEndpoint.createTask(sessionTest, 123L,"task1");
//        taskEndpoint.createTask(sessionTest, 123L,"task2");
//        Assert.assertEquals(2, taskEndpoint.getTaskList().size());
//        taskEndpoint.removeOneTaskByIndex(sessionTest, 123L,1);
//        Assert.assertEquals(1, taskEndpoint.getTaskList().size());
//        taskEndpoint.removeOneTaskByIndex(sessionTest, 123L,0);
//        Assert.assertEquals(0, taskEndpoint.getTaskList().size());
//    }
//
//    @Test
//    public void testFindOneTaskByName() {
//        @NotNull TaskDTO task1 = taskEndpoint.createTask(sessionTest, 123L,"task1");
//        @NotNull TaskDTO task2 = taskEndpoint.createTask(sessionTest, 123L,"task2");
//        @NotNull String name1 = taskEndpoint.findOneTaskByName(sessionTest, 123L,"task1").getName();
//        @NotNull String name2 = taskEndpoint.findOneTaskByName(sessionTest, 123L,"task2").getName();
//        Assert.assertEquals(task1.getName(), name1);
//        Assert.assertEquals(task2.getName(), name2);
//    }
//
//    @Test
//    public void testCreateTask() {
//        Assert.assertEquals(0, taskEndpoint.getTaskList().size());
//        taskEndpoint.createTask(sessionTest, 123L,"taskNew");
//        Assert.assertEquals(1, taskEndpoint.getTaskList().size());
//    }
//
//    @Test
//    public void testRemoveOneTaskByName() {
//        taskEndpoint.createTask(sessionTest, 123L,"task1");
//        taskEndpoint.createTask(sessionTest, 123L,"task2");
//        Assert.assertEquals(2, taskEndpoint.getTaskList().size());
//        Assert.assertNotNull(taskEndpoint.findOneTaskByName(sessionTest,123L,"task1"));
//        taskEndpoint.removeOneTaskByName(sessionTest, 123L,"task1");
//        Assert.assertNull(taskEndpoint.findOneTaskByName(sessionTest,123L,"task1"));
//        Assert.assertEquals(1, taskEndpoint.getTaskList().size());
//        Assert.assertNotNull(taskEndpoint.findOneTaskByName(sessionTest,123L,"task2"));
//        taskEndpoint.removeOneTaskByName(sessionTest, 123L,"task2");
//        Assert.assertEquals(0, taskEndpoint.getTaskList().size());
//    }
//
//    @Test
//    public void testGetTaskList() {
//        @NotNull TaskDTO task1 = taskEndpoint.createTask(sessionTest, 123L,"task1");
//        @NotNull TaskDTO task2 = taskEndpoint.createTask(sessionTest, 123L,"task2");
//        @Nullable final List<Long> expected = new ArrayList<>();
//        expected.add(task1.getId());
//        expected.add(task2.getId());
//        @NotNull final List<TaskDTO> tasks = taskEndpoint.getTaskList();
//        @NotNull final List<Long> actual = new ArrayList<>();
//        for (@NotNull final TaskDTO task: tasks) {
//            actual.add(task.getId());
//        }
//        Assert.assertEquals(expected, actual);
//    }

}