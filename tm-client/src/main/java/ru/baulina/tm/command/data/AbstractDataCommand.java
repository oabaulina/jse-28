package ru.baulina.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.command.AbstractCommand;
import ru.baulina.tm.enumerated.Role;

public abstract class AbstractDataCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[] {Role.ADMIN};
    }

}
