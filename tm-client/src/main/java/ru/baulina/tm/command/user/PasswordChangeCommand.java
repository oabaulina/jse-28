package ru.baulina.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.endpoint.SessionDTO;
import ru.baulina.tm.util.TerminalUtil;

public final class PasswordChangeCommand extends AbstractUserCommand {

    @NotNull
    @Override
    public String name() {
        return "change-password";
    }

    @NotNull
    @Override
    public String description() {
        return "Change user's password.";
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE_PASSWORD]");
        System.out.println("ENTER OLD PASSWORD:");
        final String passwordOld = TerminalUtil.nextLine();
        System.out.println("ENTER NEW PASSWORD:");
        final String passwordNew = TerminalUtil.nextLine();
        @Nullable final SessionDTO session = getSession();
        endpointLocator.getUserEndpoint().changeUserPassword(session, passwordOld, passwordNew, session.getUserId());
        System.out.println("[OK]");
        System.out.println();
    }

}
