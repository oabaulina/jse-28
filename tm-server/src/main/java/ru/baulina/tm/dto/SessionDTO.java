package ru.baulina.tm.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.entity.Session;

import javax.xml.bind.annotation.XmlRootElement;

@Getter
@Setter
@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public final class SessionDTO extends AbstractEntityDTO implements Cloneable {

    @Nullable
    @Override
    public SessionDTO clone() {
        try {
            return (SessionDTO) super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }

    @Nullable
    private Long timestamp;

    @Nullable
    private Long userId;

    @Nullable
    private String signature;

    public SessionDTO() {
    }

    @Nullable
    public SessionDTO sessionDTOfrom(@Nullable final Session session) {
        if (session == null) return null;
        @NotNull final SessionDTO sessionDTO = new SessionDTO();
        sessionDTO.setId(session.getId());
        sessionDTO.setUserId(session.getUser().getId());
        sessionDTO.setTimestamp(session.getTimestamp());
        sessionDTO.setSignature(session.getSignature());
        return sessionDTO;
    }

}
