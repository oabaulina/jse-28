package ru.baulina.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.dto.DomainDTO;

public interface IDomainService {

    void load(@Nullable final DomainDTO domain);

    void export(@Nullable final DomainDTO domain);

}
