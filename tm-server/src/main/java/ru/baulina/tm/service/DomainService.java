package ru.baulina.tm.service;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.api.service.IDomainService;
import ru.baulina.tm.api.service.IProjectService;
import ru.baulina.tm.api.service.ITaskService;
import ru.baulina.tm.api.service.IUserService;
import ru.baulina.tm.dto.DomainDTO;

@RequiredArgsConstructor
public class DomainService implements IDomainService {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IProjectService projectService;

    @NotNull
    private final ITaskService taskService;

    @Override
    public void load(@Nullable final DomainDTO domain) {
        if (domain == null) return;
        projectService.load(domain.getProjects());
        taskService.load(domain.getTasks());
        userService.load(domain.getUsers());
    }

    @Override
    public void export(@Nullable final DomainDTO domain) {
        if (domain == null) return;
        domain.setProjects(projectService.findListProjects());
        domain.setTasks(taskService.findListTasks());
        domain.setUsers(userService.findListUsers());
    }

}
