package ru.baulina.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.api.service.IPropertyService;

import javax.jws.WebMethod;
import java.io.File;
import java.io.InputStream;
import java.util.Properties;

public class PropertyService implements IPropertyService {

    @NotNull
    private final Properties properties = new Properties();

    public PropertyService() {
        try {
            init();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void init() throws Exception {
        @NotNull String NAME = "/application.properties";
        try (InputStream inputStream = PropertyService.class.getResourceAsStream(NAME)) {
            properties.load(inputStream);
        }
    }

    @Nullable
    @Override
    public String getServiceHost() {
        @Nullable final String propertyHost = properties.getProperty("server.host");
        @Nullable final String envHost = System.getProperty("server.host");
        if (envHost != null) return envHost;
        return propertyHost;
    }

    @Nullable
    @Override
    public Integer getServicePort() {
        @Nullable final String propertyPort = properties.getProperty("server.port");
        @Nullable final String envPort = System.getProperty("server.port");
        String value = propertyPort;
        if (envPort != null) value = envPort;
        return Integer.parseInt(value);
    }

    @Nullable
    @Override
    public String getSessionSalt() {
        return properties.getProperty("session.salt");
    }

    @Nullable
    @Override
    public Integer getSessionCycle() {
        return Integer.parseInt(properties.getProperty("session.cycle"));
    }

    @Nullable
    @Override
    public String getJdbcUrl() {
        return  properties.getProperty("db.type") + "://" +
                properties.getProperty("db.host") + ":" +
                properties.getProperty("db.port") + "/" +
                properties.getProperty("db.name");
    }

    @Nullable
    @Override
    public String getJdbcType() {
        return properties.getProperty("db.type");
    }

    @Nullable
    @Override
    public String getJdbcHost() {
        return properties.getProperty("db.host");
    }

    @Nullable
    @Override
    public Integer getJdbcPort() {
        return Integer.parseInt(properties.getProperty("db.port"));
    }

    @Nullable
    @Override
    public String getJdbcName() {
        return properties.getProperty("db.name");
    }

    @Nullable
    @Override
    public String getJdbcLogin() {
        return properties.getProperty("db.login");
    }

    @Nullable
    @Override
    public String getJdbcPassword() {
        return properties.getProperty("db.password");
    }

    @Nullable
    @Override
    public String getJdbcTimezone() {
        return properties.getProperty("db.timezone");
    }

    @Nullable
    @Override
    public String getJdbcDriver() {
        return properties.getProperty("db.driver");
    }

    @Nullable
    @Override
    public String getJdbcDialect() {
        return properties.getProperty("db.dialect");
    }

}
