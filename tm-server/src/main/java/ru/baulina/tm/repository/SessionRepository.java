package ru.baulina.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.api.repository.ISessionRepository;
import ru.baulina.tm.dto.SessionDTO;
import ru.baulina.tm.entity.Session;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.List;

public final class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    @NotNull private final EntityManager entityManager;

    public SessionRepository(@NotNull EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public List<Session> getListSession() {
        @NotNull final String sql = "SELECT s FROM Session s";
        @NotNull final TypedQuery<Session> query =
                entityManager.createQuery(sql, Session.class);
        return query.getResultList();
    }

    @Override
    public void remove(@NotNull final SessionDTO session) {
        @NotNull Long id = session.getId();
        entityManager.remove(entityManager.find(Session.class, id));
     }

    @Override
    public void removeByUserId(@NotNull final Long userId) {
        @Nullable final List<Session> sessions = findByUserId(userId);
        for (@NotNull final Session session: sessions) {
            remove((new SessionDTO()).sessionDTOfrom(session));
        }
    }

    @NotNull
    @Override
    public List<Session> findByUserId(@NotNull final Long userId) {
        @NotNull final String sql = "SELECT s FROM Session s WHERE s.user.id = :userId";
        @NotNull final TypedQuery<Session> query =
                entityManager.createQuery(sql, Session.class)
                        .setParameter("userId", userId);
        return query.getResultList();
    }

    @Nullable
    @Override
    public Session findById(@NotNull final Long id) {
        @NotNull final String sql = "SELECT s FROM Session s WHERE s.id = :id";
        @NotNull final TypedQuery<Session> query =
                entityManager.createQuery(sql, Session.class)
                        .setParameter("id", id);
        return query.getResultList().get(0);
    }

    @Override
    public boolean contains(@NotNull final Long id) {
        @NotNull final String sql = "SELECT count(s) FROM Session s WHERE s.id = :id ";
        @NotNull final TypedQuery<Long> query = entityManager.createQuery(sql, Long.class)
                .setParameter("id", id);
        return (query.getSingleResult() > 0);
    }

}
